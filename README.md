# TTM00-FW-PIC32-Training-Week2

This repo is for training purposes

1. Project Brief
    This project is for training purposes from the Titoma Company and is about different topics related to the programming of microcontrollers (Week2).
2. Hardware Description
    - PIC32MX470512H: "https://ww1.microchip.com/downloads/en/DeviceDoc/PIC32MX330350370430450470_Datasheet_DS60001185H.pdf"
    - Curiosity board user guide: "http://ww1.microchip.com/downloads/en/DeviceDoc/70005283B.pdf"
3. Serial commands
    All serial commands must star with '*' and end with '#'.
    - ECHO Command: Sending *ECHO# to the PIC32 makes it respond the same message.
    - Modify period and DC: Sending *AABB#, being AA the percentage of the period for the LED to stay on, thus, 80 means 800ms and 200ms of the LED turned down.
    - Modify period and DC: Sending *AABB#, being BB the percentage of the DC for the LED brightness, thus, 80 means 80% of bright.
4. Prerequisites
    1. SDK Version
        - IDE Version: 5.50
        - Compiler version: v1.42
        - Project configuration: 
            - Categories: Microchip Embedded
            - Projects: Standalone Project
            - Family: All families
            - Device: PIC32MX470F512H
            - Hardward tools: Microchip starter kits
            - Compiler: XC32 (v1.42)
            - Encoding: ISO-8859-1
5. Versioning
    1. Current version of the FW
        - V1.0.20210713
6. Authors
    1. Project staff: "Miguel Angel Arias Giraldo"
    2. Maintainer contact email: "miguelariasgiraldo2000@gmail.com"